**Three infallible tricks to improve your homemade pizzas**

If you love to enjoy a good homemade pizza, improve the final result using the recommendations of chef Pino Prestanizzi, a Calabrian chef who runs one of the best classic pizzerias in Barcelona, and who also has a YouTube channel.

Made at home and with fresh and healthy ingredients, pizza can be a healthy and complete occasional dish, ideal to share with your loved ones. If you want to surprise them, you can use the expert advice of Pino Prestanzzini, an Italian pizza maker who currently has 788,000 subscribers on YouTube and 78,000 followers on Instagram.

In his social networks and YouTube videos, Prestanzzini solves viral challenges, tastes products, cooks with famous youtubers and shares recipes to make at home such as pasta carbonara, authentic lasagna, tiramisu, focaccia, pesto and of course, pizza, with its corresponding homemade dough. Today we will give you three of his tricks -which have millions of reproductions behind- to make the pizzas come out like a real Italian chef.
The trick for the perfect dough in three minutes

The dough must have the following ingredients:  500 grams of type 00 flour, 350 centiliters of water, 10 grams of yeast (one tablespoon), 20 grams of salt and a good jet of virgin olive oil . Mix the yeast with the flour, add half the water, mix, the pinch of salt, continue mixing, add the jet of oil and the remaining water and start to knead with those hands until there is a ball that does not stick to you to the hands.

The secret? Leave it covered and covered with a cloth for two hours so that it is light, without even a hint of air entering it.
How are ecological, biological, [organic and sustainable foods](https://murshidalam.com/how-are-ecological-biological-organic-and-sustainable-foods-different/) different?

**Ideal oven temperature**

You won't be lucky enough to have a wood-burning oven like the one in this image, but yours is an electric one too. It is important to oil the tray well without fear and distribute the dough in the shape of the tray by tapping it gently with your fingertips. 
The oven should be on at the maximum temperature, the ideal would be 300 degrees, or failing that, at the maximum temperature, which will take a little longer to do. Add oregano, basil, oil and salt to the tomato sauce and add it to the dough and put it in the oven, at first without any other ingredients, for a few minutes - it depends a lot on the power of the oven, between five and ten minutes is what habitual-.

The perfect time to add the cheese ... and also the oregano
Remember to add the oregano to the tomato sauce, and if you want to add fresh oregano on top, do it a few minutes before taking the pizza out of the oven.
Remove the pizza halfway through baking to add the cheese to your liking - this will keep the cheese from burning. The classic is mozzarella, you can also add peppers, mushrooms, capers, onion ... whatever you want.
